package ru.t1.chernysheva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.repository.model.IProjectRepository;
import ru.t1.chernysheva.tm.comparator.CreatedComparator;
import ru.t1.chernysheva.tm.comparator.NameComparator;
import ru.t1.chernysheva.tm.comparator.StatusComparator;
import ru.t1.chernysheva.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.Comparator;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository  {

    public ProjectRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected Class<Project> getEntityClass() {
        return Project.class;
    }

}

