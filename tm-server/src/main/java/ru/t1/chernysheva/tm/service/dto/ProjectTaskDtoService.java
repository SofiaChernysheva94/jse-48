package ru.t1.chernysheva.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.chernysheva.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.chernysheva.tm.api.service.IConnectionService;
import ru.t1.chernysheva.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.chernysheva.tm.dto.model.ProjectDTO;
import ru.t1.chernysheva.tm.dto.model.TaskDTO;
import ru.t1.chernysheva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chernysheva.tm.exception.entity.TaskNotFoundException;
import ru.t1.chernysheva.tm.exception.field.IndexIncorrectException;
import ru.t1.chernysheva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.chernysheva.tm.exception.field.TaskIdEmptyException;
import ru.t1.chernysheva.tm.exception.field.UserIdEmptyException;
import ru.t1.chernysheva.tm.repository.dto.ProjectDtoRepository;
import ru.t1.chernysheva.tm.repository.dto.TaskDtoRepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskDtoService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final TaskDTO task;
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final List<TaskDTO> tasks;
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            tasks = taskRepository.findAllByProjectId(userId, projectId);
            entityManager.getTransaction().begin();
            for (final TaskDTO task : tasks) taskRepository.removeById(userId, task.getId());
            projectRepository.removeById(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectByIndex(@Nullable final String userId, @Nullable final Integer projectIndex) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectIndex == null || projectIndex < 1) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final List<TaskDTO> tasks;
        @Nullable ProjectDTO project;
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            project = projectRepository.findOneByIndex(userId, projectIndex);
            if (project == null) throw new ProjectNotFoundException();
            tasks = taskRepository.findAllByProjectId(userId, project.getId());
            entityManager.getTransaction().begin();
            for (final TaskDTO task : tasks) taskRepository.removeById(userId, task.getId());
            projectRepository.removeById(userId, project.getId());
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unbindTaskFromProject(@Nullable final String userId,
                                      @Nullable final String projectId,
                                      @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final TaskDTO task;
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
