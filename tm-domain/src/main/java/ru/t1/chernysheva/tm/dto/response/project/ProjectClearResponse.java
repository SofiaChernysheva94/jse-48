package ru.t1.chernysheva.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.chernysheva.tm.dto.response.user.AbstractUserResponse;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectClearResponse extends AbstractUserResponse {
}
