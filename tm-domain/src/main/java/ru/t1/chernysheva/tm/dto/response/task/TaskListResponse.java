package ru.t1.chernysheva.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.model.TaskDTO;

import java.util.List;

@NoArgsConstructor
public final class TaskListResponse extends AbstractTaskResponse {


    @Getter
    @Setter
    @Nullable
    private List<TaskDTO> tasks;

    public TaskListResponse(List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

}
