package ru.t1.chernysheva.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.AbstractUserRequest;
import ru.t1.chernysheva.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sortType;

    public ProjectListRequest(@Nullable final String token, @Nullable Sort sortType) {
        super(token);
        this.sortType = sortType;
    }

    public ProjectListRequest(@Nullable final String token) {
        super(token);
    }

}
