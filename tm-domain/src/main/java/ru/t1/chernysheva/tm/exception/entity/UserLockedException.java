package ru.t1.chernysheva.tm.exception.entity;

import ru.t1.chernysheva.tm.exception.AbstractException;

public class UserLockedException extends AbstractException {
    public UserLockedException() {
        super("Error! User is locked...");
    }

}
