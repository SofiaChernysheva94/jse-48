package ru.t1.chernysheva.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
public final class TaskRemoveByIdResponse extends AbstractTaskResponse {

    public TaskRemoveByIdResponse(@NotNull final TaskDTO task) {
        super(task);
    }

}
