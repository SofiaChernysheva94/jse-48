package ru.t1.chernysheva.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.chernysheva.tm.dto.response.user.AbstractUserResponse;
import ru.t1.chernysheva.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
public final class TaskChangeStatusByIdResponse extends AbstractUserResponse {

    private TaskDTO task;

    public TaskChangeStatusByIdResponse(TaskDTO task) {
        this.task = task;
    }

}
