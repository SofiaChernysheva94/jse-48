package ru.t1.chernysheva.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.AbstractUserRequest;
import ru.t1.chernysheva.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sortType;

    public TaskListRequest(@Nullable final String token, @Nullable final Sort sortType) {
        super(token);
        this.sortType = sortType;
    }

    public TaskListRequest(@Nullable final String token) {
        super(token);
    }

}
