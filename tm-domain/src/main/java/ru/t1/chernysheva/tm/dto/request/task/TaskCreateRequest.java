package ru.t1.chernysheva.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskCreateRequest extends AbstractUserRequest {

    private String name;

    private String description;

    public TaskCreateRequest(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public TaskCreateRequest(@Nullable final String token) {
        super(token);
    }

}
