package ru.t1.chernysheva.tm.api.model;

import org.jetbrains.annotations.Nullable;

public interface IHasDescription {

    @Nullable
    String getDescription();

}
