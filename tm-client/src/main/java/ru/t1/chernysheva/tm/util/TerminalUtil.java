package ru.t1.chernysheva.tm.util;

import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }
    @NotNull
    static Integer nextNumber() {
        @NotNull final String value = nextLine();
        return Integer.parseInt(value);
    }
}
